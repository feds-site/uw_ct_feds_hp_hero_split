<?php

/**
 * @file
 * uw_ct_feds_hp_hero_split.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_hp_hero_split_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_homepage_hero_split content'.
  $permissions['create feds_homepage_hero_split content'] = array(
    'name' => 'create feds_homepage_hero_split content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any feds_homepage_hero_split content'.
  $permissions['delete any feds_homepage_hero_split content'] = array(
    'name' => 'delete any feds_homepage_hero_split content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_homepage_hero_split content'.
  $permissions['delete own feds_homepage_hero_split content'] = array(
    'name' => 'delete own feds_homepage_hero_split content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any feds_homepage_hero_split content'.
  $permissions['edit any feds_homepage_hero_split content'] = array(
    'name' => 'edit any feds_homepage_hero_split content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_homepage_hero_split content'.
  $permissions['edit own feds_homepage_hero_split content'] = array(
    'name' => 'edit own feds_homepage_hero_split content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'enter feds_homepage_hero_split revision log entry'.
  $permissions['enter feds_homepage_hero_split revision log entry'] = array(
    'name' => 'enter feds_homepage_hero_split revision log entry',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_split authored by option'.
  $permissions['override feds_homepage_hero_split authored by option'] = array(
    'name' => 'override feds_homepage_hero_split authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_split authored on option'.
  $permissions['override feds_homepage_hero_split authored on option'] = array(
    'name' => 'override feds_homepage_hero_split authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_split comment setting option'.
  $permissions['override feds_homepage_hero_split comment setting option'] = array(
    'name' => 'override feds_homepage_hero_split comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_split promote to front page option'.
  $permissions['override feds_homepage_hero_split promote to front page option'] = array(
    'name' => 'override feds_homepage_hero_split promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_split published option'.
  $permissions['override feds_homepage_hero_split published option'] = array(
    'name' => 'override feds_homepage_hero_split published option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_split revision option'.
  $permissions['override feds_homepage_hero_split revision option'] = array(
    'name' => 'override feds_homepage_hero_split revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_homepage_hero_split sticky option'.
  $permissions['override feds_homepage_hero_split sticky option'] = array(
    'name' => 'override feds_homepage_hero_split sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_homepage_hero_split content'.
  $permissions['search feds_homepage_hero_split content'] = array(
    'name' => 'search feds_homepage_hero_split content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
