<?php

/**
 * @file
 * uw_ct_feds_hp_hero_split.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_hp_hero_split_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_hp_hero_split_node_info() {
  $items = array(
    'feds_homepage_hero_split' => array(
      'name' => t('Feds homepage hero split'),
      'base' => 'node_content',
      'description' => t('A split-screen hero area displayed at the top of the front page directly under the navigation. This areas contains a title, content, image, and optional call-to-action button.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_feds_hp_hero_split_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: comment_node_feds_homepage_hero_split
  $schemaorg['comment']['comment_node_feds_homepage_hero_split'] = array(
    'rdftype' => array(
      0 => 'sioc:Post',
      1 => 'sioct:Comment',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'comment_body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'pid' => array(
      'predicates' => array(
        0 => 'sioc:reply_of',
      ),
      'type' => 'rel',
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
  );

  // Exported RDF mapping: feds_homepage_hero_split
  $schemaorg['node']['feds_homepage_hero_split'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_main_hero_image' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_background_colour_left' => array(
      'predicates' => array(),
    ),
    'field_hero_image_right' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_background_colour_right' => array(
      'predicates' => array(),
    ),
    'field_primary_image' => array(
      'predicates' => array(),
    ),
    'field_left_side_title' => array(
      'predicates' => array(),
    ),
    'field_left_side_body_content' => array(
      'predicates' => array(),
    ),
    'field_right_side_title' => array(
      'predicates' => array(),
    ),
    'field_right_side_body' => array(
      'predicates' => array(),
    ),
    'field_left_heading' => array(
      'predicates' => array(),
    ),
    'field_right_heading' => array(
      'predicates' => array(),
    ),
    'field_hero_heading' => array(
      'predicates' => array(),
    ),
    'field_hero_body_content' => array(
      'predicates' => array(),
    ),
    'field_show_cta' => array(
      'predicates' => array(),
    ),
    'field_button_text' => array(
      'predicates' => array(),
    ),
    'field_cta_button_theme' => array(
      'predicates' => array(),
    ),
    'field_button_theme_class_name' => array(
      'predicates' => array(),
    ),
    'field_cta_hero_link' => array(
      'predicates' => array(),
    ),
    'field_hero_subtitle' => array(
      'predicates' => array(),
    ),
    'field_flexible_heading' => array(
      'predicates' => array(),
    ),
    'field_hero_content_text_colour' => array(
      'predicates' => array(),
    ),
  );

  return $schemaorg;
}
